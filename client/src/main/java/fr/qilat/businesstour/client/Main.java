package fr.qilat.businesstour.client;

import fr.qilat.businesstour.client.console.ClientInputHandler;
import fr.qilat.businesstour.server.console.ConsoleThread;
import fr.qilat.businesstour.server.netty.client.NettyClientThread;
import lombok.Getter;

public class Main {
    
    public static final String HOST = "192.168.1.21";
    public static final int PORT = 24462;
    
    private static Thread mainThread;
    @Getter
    private static NettyClientThread nettyThread;
    private static ConsoleThread consoleThread;
    
    public static void main(String[] args) {
        mainThread = Thread.currentThread();
        
        nettyThread = new NettyClientThread(HOST, PORT);
        nettyThread.start();
        
        consoleThread = new ConsoleThread(new ClientInputHandler());
        consoleThread.start();
    }
    
    public static void stop() throws InterruptedException {
        if (nettyThread != null) {
            nettyThread.prepareStop();
            nettyThread.join();
        }
    }
}
