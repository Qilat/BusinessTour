package fr.qilat.businesstour.client.console;

import fr.qilat.businesstour.client.Main;
import fr.qilat.businesstour.server.console.InputHandler;
import fr.qilat.businesstour.server.netty.packet.HandshakePacket;

public class ClientInputHandler implements InputHandler {
    @Override
    public boolean handle(String input) {
        if (input.startsWith("send ")){
            String msg = input.replaceFirst("send ", "");
            HandshakePacket hp = new HandshakePacket();
            hp.setMsg(msg);
            Main.getNettyThread().getPacketClient().send(hp);
        }
        return true;
    }
    
    @Override
    public void onStop() {
        try {
            Main.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
