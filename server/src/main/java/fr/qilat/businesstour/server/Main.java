package fr.qilat.businesstour.server;

import fr.qilat.businesstour.server.console.ConsoleThread;
import fr.qilat.businesstour.server.console.ServerInputHandler;
import fr.qilat.businesstour.server.netty.server.NettyServerThread;

public class Main {
    
    private static final int PORT = 24462;
    
    private static Thread mainThread;
    private static NettyServerThread nettyThread;
    private static ConsoleThread consoleThread;
    
    public static void main(String[] args) {
        mainThread = Thread.currentThread();
        
        nettyThread = new NettyServerThread(PORT);
        nettyThread.start();
        
        consoleThread = new ConsoleThread(new ServerInputHandler());
        consoleThread.start();
    }
    
    public static void stop() throws InterruptedException {
        if (nettyThread != null) {
            nettyThread.prepareStop();
            nettyThread.join();
        }
    }
}
