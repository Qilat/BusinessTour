package fr.qilat.businesstour.server.netty.packet;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.nio.charset.StandardCharsets;

@NoArgsConstructor
public class HandshakePacket extends Packet {
    
    public static final int ID = 0x1;
    @Getter
    @Setter
    private String msg;
    
    @Override
    public int getId() {
        return ID;
    }
    
    @Override
    public void in(ByteBuf in) {
        int strLen = in.readInt();
        msg = in.readCharSequence(strLen, StandardCharsets.UTF_8).toString();
    }
    
    @Override
    public void out(ByteBuf out) {
        out.writeInt(this.msg.length());
        out.writeCharSequence(this.msg, StandardCharsets.UTF_8);
    }
    
}
