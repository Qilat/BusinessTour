package fr.qilat.businesstour.server.netty.handlers;

import fr.qilat.businesstour.server.ClientList;
import fr.qilat.businesstour.server.netty.packet.HandshakePacket;
import fr.qilat.businesstour.server.netty.packet.Packet;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class PacketHandler extends ChannelInboundHandlerAdapter {
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        Packet packet = (Packet) msg;
        
        if (packet instanceof HandshakePacket) {
            System.out.println("Received = " + ((HandshakePacket) packet).getMsg());
            HandshakePacket response = new HandshakePacket();
            response.setMsg("OK");
            ctx.writeAndFlush(response);
    
            ClientList.list.add(ctx.channel());
            
        }
    }
}
