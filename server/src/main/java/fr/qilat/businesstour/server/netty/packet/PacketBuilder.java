package fr.qilat.businesstour.server.netty.packet;

import java.util.HashMap;

public class PacketBuilder {
    
    private static final HashMap<Integer, Class<? extends Packet>> packetClassMap = new HashMap<>();
    
    static {
        packetClassMap.put(HandshakePacket.ID, HandshakePacket.class);
        
    }
    
    
    public static Packet build(int packetTypeId) throws IllegalAccessException, InstantiationException {
        Class<? extends Packet> clazz = packetClassMap.get(packetTypeId);
        return clazz.newInstance();
    }
    
}
