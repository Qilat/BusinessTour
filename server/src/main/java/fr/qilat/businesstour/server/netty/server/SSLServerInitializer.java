package fr.qilat.businesstour.server.netty.server;

import fr.qilat.businesstour.server.netty.handlers.PacketDecoder;
import fr.qilat.businesstour.server.netty.handlers.PacketEncoder;
import fr.qilat.businesstour.server.netty.handlers.PacketHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

import javax.net.ssl.SSLException;
import java.security.cert.CertificateException;

/**
 * TODO
 */
public class SSLServerInitializer extends ChannelInitializer<SocketChannel> {
    
    private SslContext sslCtx;
    
    public SSLServerInitializer(boolean ssl) throws CertificateException, SSLException {
        if (ssl) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            this.sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
        }
    }
    
    @Override
    protected void initChannel(SocketChannel channel) {
        ChannelPipeline pipeline = channel.pipeline();
        if(this.sslCtx != null)
            pipeline.addLast("ssl", sslCtx.newHandler(channel.alloc()));

        pipeline.addLast("decoder", new PacketDecoder());
        pipeline.addLast("encoder", new PacketEncoder());
        pipeline.addLast("handler", new PacketHandler());
    }
}
