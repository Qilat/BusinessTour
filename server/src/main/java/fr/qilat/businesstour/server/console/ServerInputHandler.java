package fr.qilat.businesstour.server.console;

import fr.qilat.businesstour.server.ClientList;
import fr.qilat.businesstour.server.Main;
import fr.qilat.businesstour.server.netty.packet.HandshakePacket;
import io.netty.channel.Channel;

public class ServerInputHandler implements InputHandler {
    @Override
    public boolean handle(String input) {
        
        if (input.startsWith("send ")) {
            input = input.replaceFirst("send ", "");
            int nb = Integer.parseInt(input.charAt(0) + "");
            input = input.substring(2);
            
            Channel channel = ClientList.list.get(nb);
            
            if (channel != null) {
                HandshakePacket packet = new HandshakePacket();
                packet.setMsg(input);
                channel.writeAndFlush(packet);
            }
            
        }
        return true;
    }
    
    @Override
    public void onStop() {
        try {
            Main.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
