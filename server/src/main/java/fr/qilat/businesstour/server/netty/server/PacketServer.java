package fr.qilat.businesstour.server.netty.server;

import fr.qilat.businesstour.server.netty.handlers.PacketDecoder;
import fr.qilat.businesstour.server.netty.handlers.PacketEncoder;
import fr.qilat.businesstour.server.netty.handlers.PacketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Getter;

public class PacketServer {
    
    @Getter
    private final int port;
    private ChannelFuture future;
    
    public PacketServer(int port) {
        this.port = port;
    }
    
    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
       
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                                @Override
                                protected void initChannel(SocketChannel channel) {
                                    channel.pipeline().addLast(
                                            new PacketDecoder(),
                                            new PacketEncoder(),
                                            new PacketHandler()
                                    );
                                }
                            }
                    )
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            this.future = b.bind(this.port).sync();
            this.future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
    
    public void stop() {
        this.future.addListener(ChannelFutureListener.CLOSE);
    }
    
}
