package fr.qilat.businesstour.server.netty.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.ipfilter.AbstractRemoteAddressFilter;

import java.net.InetSocketAddress;

public class IpFilter extends AbstractRemoteAddressFilter<InetSocketAddress> {
    
    @Override
    protected boolean accept(ChannelHandlerContext channelHandlerContext, InetSocketAddress inetSocketAddress) throws Exception {
        return false;
    }
}
