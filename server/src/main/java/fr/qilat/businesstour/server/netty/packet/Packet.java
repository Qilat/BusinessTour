package fr.qilat.businesstour.server.netty.packet;

import io.netty.buffer.ByteBuf;

public abstract class Packet {
    
    /**
     * Fill packet instance with Bytebuff data
     *
     * @param in ByteBuf instance containing data
     */
    public abstract void in(ByteBuf in);
    
    /**
     * Fill Bytebuf instance with packet data
     *
     * @param out ByteBuf which must me fill with data
     */
    public abstract void out(ByteBuf out);
    
    public abstract int getId();
    
}
