package fr.qilat.businesstour.server.netty.handlers;

import fr.qilat.businesstour.server.netty.packet.Packet;
import fr.qilat.businesstour.server.netty.packet.PacketBuilder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

public class PacketDecoder extends ReplayingDecoder<Packet> {
    
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        Packet packet = PacketBuilder.build(in.readInt());
        packet.in(in);
        out.add(packet);
    }
    
}
