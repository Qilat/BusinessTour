package fr.qilat.businesstour.server.console;

import java.util.Scanner;

public class ConsoleThread extends Thread {
    
    private final InputHandler handler;
    
    public ConsoleThread(InputHandler handler) {
        super("Console");
        this.handler = handler;
    }
    
    @Override
    public void run() {
        boolean shouldContinue = true;
        Scanner scanner = new Scanner(System.in);
        
        do {
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("stop")) {
                shouldContinue = false;
            } else {
                this.handler.handle(input);
            }
        } while (shouldContinue);
        
        this.handler.onStop();
        
    }
}
