package fr.qilat.businesstour.server.netty.server;

import lombok.Getter;

public class NettyServerThread extends Thread{
    
    private final int port;
    @Getter
    private PacketServer packetServer;
    
    public NettyServerThread(int port) {
        super("NettyServer");
        this.port = port;
    }
    
    @Override
    public void run() {
        this.packetServer = new PacketServer(port);
        try {
            this.packetServer.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void prepareStop() {
        this.packetServer.stop();
    }
    
}
