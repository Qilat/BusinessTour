package fr.qilat.businesstour.server.netty.client;

import lombok.Getter;

public class NettyClientThread extends Thread {
    
    private final String host;
    private final int port;
    @Getter
    private PacketClient packetClient;
    
    public NettyClientThread(String host, int port) {
        super("NettyClient");
        this.host = host;
        this.port = port;
    }
    
    @Override
    public void run() {
        this.packetClient = new PacketClient(host, port);
        try {
            this.packetClient.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void prepareStop() {
        this.packetClient.stop();
    }
}
