package fr.qilat.businesstour.server.netty.client;

import fr.qilat.businesstour.server.netty.packet.Packet;
import fr.qilat.businesstour.server.netty.packet.HandshakePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class PacketClientHandler extends ChannelInboundHandlerAdapter {
    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        HandshakePacket hp = new HandshakePacket();
        hp.setMsg("Hello bro !");
        ctx.writeAndFlush(hp);
    }
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        Packet packet = (Packet) msg;
        
        if(packet instanceof HandshakePacket){
            System.out.println("Msg received = " + ((HandshakePacket) packet).getMsg());
        }
        
    }
}
