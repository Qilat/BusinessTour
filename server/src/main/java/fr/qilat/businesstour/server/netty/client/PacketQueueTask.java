package fr.qilat.businesstour.server.netty.client;

import fr.qilat.businesstour.server.netty.packet.Packet;

import java.util.LinkedList;
import java.util.TimerTask;

public class PacketQueueTask extends TimerTask {
    
    private final PacketClient client;
    private final LinkedList<Packet> queue = new LinkedList<>();
    
    public PacketQueueTask(PacketClient client) {
        super();
        this.client = client;
    }
    
    @Override
    public void run() {
        if (!this.queue.isEmpty() && this.client.getFuture().channel().isWritable()) {
           this.client.getFuture().channel().writeAndFlush(queue.pollFirst());
        }
    }
    
    public void addPacketToQueue(Packet packet) {
        this.queue.addLast(packet);
    }
}
