package fr.qilat.businesstour.server.netty.client;

import fr.qilat.businesstour.server.netty.packet.Packet;
import fr.qilat.businesstour.server.netty.handlers.PacketDecoder;
import fr.qilat.businesstour.server.netty.handlers.PacketEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.AccessLevel;
import lombok.Getter;

import java.net.InetAddress;
import java.util.Timer;

public class PacketClient {
    @Getter
    private final String host;
    @Getter
    private final int port;
    @Getter(AccessLevel.PROTECTED)
    private ChannelFuture future;
    private Timer timer;
    private PacketQueueTask queueTask;
    
    public PacketClient(String host, int port) {
        this.host = host;
        this.port = port;
    }
    
    public void run() throws Exception {
        this.queueTask = new PacketQueueTask(this);
        this.timer = new Timer();
        this.timer.schedule(this.queueTask, 20);
        
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        
        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(
                    new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline().addLast(
                                    new PacketEncoder(),
                                    new PacketDecoder(),
                                    new PacketClientHandler()
                            );
                        }
                    }
            );
            
            future = b.connect(InetAddress.getByName(this.host), this.port).sync();
            future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
        }
    }
    
    //todo test
    public void send(Packet packet) {
        Channel channel = this.future.channel();
        if (channel.isWritable()){
            channel.writeAndFlush(packet);
        } else {
            this.queueTask.addPacketToQueue(packet);
        }
    }
    
    public void stop() {
        this.timer.cancel();
        this.future.addListener(ChannelFutureListener.CLOSE);
    }
    
}
