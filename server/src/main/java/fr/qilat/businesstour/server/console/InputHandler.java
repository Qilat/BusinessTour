package fr.qilat.businesstour.server.console;

public interface InputHandler {
    
    boolean handle(String input);
    
    void onStop();
    
}
